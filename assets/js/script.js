let dropdown_menu = document.querySelector('.dropdown');
let searchForm = document.querySelector('.search-form');
let overlay = document.querySelector('#backgroundOverlay');

document.querySelector('#search-btn').onclick = () => {
    searchForm.classList.toggle('active');
    overlay.classList.toggle('active');
    dropdown_menu.classList.remove('active');
}

document.querySelector('#dropdown-icon').onclick = () => {
    overlay.classList.toggle('active');
    dropdown_menu.classList.toggle('active');
    searchForm.classList.remove('active');
}

document.querySelector('#backgroundOverlay').onclick = () => {
    dropdown_menu.classList.remove('active');
    overlay.classList.remove('active');
    searchForm.classList.remove('active');
}


window.onscroll = () => {

    dropdown_menu.classList.remove('active');
    searchForm.classList.remove('active');

    if (window.scrollY > 80) {
        document.querySelector('.header .header-2').classList.add('active');
    }
    else {
        document.querySelector('.header .header-2').classList.remove('active');
    }
}

window.onload = () => {
    if (window.scrollY > 80) {
        document.querySelector('.header .header-2').classList.add('active');
    }
    else {
        document.querySelector('.header .header-2').classList.remove('active');
    }
}

var swiper = new Swiper(".bikes-slider", {
    loop: true,
    centerSlides: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    breakpoints: {
        0: {
            slidesPerView: 1,
        },
        768: {
            slidesPerView: 1,
        },
        1024: {
            slidesPerView: 1,
        },
    },
});