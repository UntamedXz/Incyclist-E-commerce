<header class="header">
    <div class="header-1">
        <a href="index.php" class="logo">iNCYCLIST</a>
        <form action="#" class="search-form">
            <input type="search" name="" placeholder="search here..." id="search-box">
            <label for="search-box" class="fas fa-search"></label>
        </form>
        <div class="icons">
            <div id="search-btn" class="fas fa-search"></div>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-shopping-cart"></a>
            <div id="login-btn" class="fas fa-user"></div>
        </div>
    </div>
    <div class="header-2">
        <nav class="custom-navbar shadow-sm">
            <!-- <a href="index.php">home</a> -->
            <a href="accessories.php">accessories</a>
            <a href="bikes.php">bikes</a>
            <a href="#">clothing</a>
            <a href="#">components</a>
            <a href="#">services</a>
            <a href="#">rentals</a>
        </nav>
    </div>
</header>

<nav class="bottom-navbar">
    <!-- <a href="index.php" class="fas fa-home"></a> -->
    <a href="accessories.php" class="fa-solid fa-traffic-light"></a>
    <a href="bikes.php" class="fa-solid fa-bicycle"></a>
    <a href="#" class="fa-solid fa-shirt"></a>
    <a href="#" class="fa-solid fa-helmet-safety"></a>
    <a href="#" class="fa-solid fa-screwdriver-wrench"></a>
    <a href="#" class="fa-solid fa-person-biking"></a>
</nav>